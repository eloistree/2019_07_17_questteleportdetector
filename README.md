# How to use: Quest Teleport Detector   
   
Add the following line to the [UnityRoot]/Packages/manifest.json    
``` json     
"be.eloiexperiments.questteleportdetector":"https://gitlab.com/eloistree/2019_07_17_questteleportdetector.git",    
```    
--------------------------------------    
   
Feel free to support my work: http://patreon.com/eloistree   
Contact me if you need assistance: http://eloistree.page.link/discord   
   
--------------------------------------    
``` json     
{                                                                                
  "name": "be.eloiexperiments.questteleportdetector",                              
  "displayName": "Quest Teleport Detector",                        
  "version": "0.0.1",                         
  "unity": "2018.1",                             
  "description": "Detect if the user teleport from one point to an other in the Oculus area because of a recenter or a lag.",                         
  "keywords": ["Script","Tool","init"],                       
  "category": "Script",                   
  "dependencies":{"be.eloiexperiments.randomtool": "0.0.1"}     
  }                                                                                
```    