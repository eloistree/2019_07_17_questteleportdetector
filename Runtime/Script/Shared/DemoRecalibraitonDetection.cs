﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoRecalibraitonDetection : MonoBehaviour
{
    public AudioSource m_teleportation;
    public AudioSource m_recalibration;
    public GameObject m_prefab;
    
    public void PlayTeleportationSound()
    {
        if(m_teleportation)
        m_teleportation.Play();
    }

    public void PlayRecalibrationSound()
    {
        if (m_recalibration)
            m_recalibration.Play();

    }

    public void CreatePrefabAtTeleportationSource(TeleportationInfo teleportation) {

        GameObject obj = null;
        if (m_prefab == null)
            obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        else obj = GameObject.Instantiate(m_prefab);
        
                obj.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
        obj.transform.position = teleportation.m_positionFrom;
        obj.transform.rotation = teleportation.m_rotationFrom;
        


        if (m_prefab == null)
            obj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        else obj = GameObject.Instantiate(m_prefab);
        obj.transform.localScale = new Vector3(0.03f, 0.03f, 0.03f);
        obj.transform.position = teleportation.m_positionTo;
        obj.transform.rotation = teleportation.m_rotationTo;
    }
}
