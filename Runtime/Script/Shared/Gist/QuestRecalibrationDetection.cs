﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuestRecalibrationDetection : MonoBehaviour
{
    public Transform m_playerHead;
    public float m_distanceToBeCheckAsTeleport=0.1f;
    public OnTeleportationDetected m_teleportationDetected;
    public OnTeleportationDetected m_recalibrationDetected;

    [Header("Debug")]
    public Vector3 previousLocalPosition;
    public Vector3    previousWorldPosition;
    public Quaternion previousWorldRotation;
    
    void Update()
    {
       
        Vector3 newPosition = m_playerHead.localPosition;
        if (Vector3.Distance(previousLocalPosition, newPosition) > m_distanceToBeCheckAsTeleport && IsNotFirstFrameOfTheLevel())
        {
            TeleportationInfo teleport = new TeleportationInfo(previousWorldPosition, previousWorldRotation, GetCurrentPosition(), GetCurrentRotation());
            m_teleportationDetected.Invoke(teleport);
            Vector3 newPositionOn2DPlan = newPosition;
            newPositionOn2DPlan.y = 0;
            if (Vector3.Distance(newPositionOn2DPlan, Vector3.zero) < 0.05)
                m_recalibrationDetected.Invoke(teleport);
        }
        previousLocalPosition = newPosition;
        previousWorldPosition = GetCurrentPosition();
        previousWorldRotation = GetCurrentRotation();
    }

    private static bool IsNotFirstFrameOfTheLevel()
    {
        return Time.timeSinceLevelLoad > 1f;
    }

    private Quaternion GetCurrentRotation()
    {
       return m_playerHead.rotation;
    }

    private Vector3 GetCurrentPosition()
    {
        return m_playerHead.position;
    }
}
public struct TeleportationInfo
{
    public Vector3 m_positionFrom;
    public Vector3 m_positionTo;
    public Quaternion m_rotationFrom;
    public Quaternion m_rotationTo;

    public TeleportationInfo(Vector3 previousWorldPosition, Quaternion previousWorldRotation, Vector3 newWorldPosition, Quaternion newWorldRotation) : this()
    {
        this.m_positionFrom = previousWorldPosition;
        this.m_rotationFrom = previousWorldRotation;
        this.m_positionTo = newWorldPosition;
        this.m_rotationTo = newWorldRotation;
    }

   
}
[System.Serializable]
public class OnTeleportationDetected : UnityEvent<TeleportationInfo>{}
